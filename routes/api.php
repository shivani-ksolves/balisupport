<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', array('as' => 'login', 'uses' => 'UsersController@login'));

Route::group(['middleware'=>['cors']], function() {

	Route::get('/getSocialLinks', 'ServiceController@getSocialLinks'); //Get social media links
	Route::get('/getFaq', 'FaqController@getFaq')->name('faq_api'); //Get faq
	Route::get('/getServices','ServiceController@getServices');
	Route::get('/getHospitals','HospitalController@getHospitals');
	Route::get('/getContactUsInfo','SettingsController@getContactUsInfo');
	Route::post('/putQuery','QueryController@insertQuery');
	Route::post('/signup','UsersController@userSignup');
	//Route::post('login','UsersController@userLogin');
	Route::get('/getAtms','AtmsController@getAtmInfo')->name('atm_api'); //Get ATMs
	Route::get('/getBanks','AtmsController@getAtmInfo')->name('bank_api'); //Get Bank
	Route::get('/getHolidays','HolidaysController@getHolidays'); //Get Holidays
	Route::get('/getAlert','TravelAlertController@getAlert'); //Get Alerts

});	