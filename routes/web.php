<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('login');
})->middleware('loginAuth');


Route::get('/login', function () {
    return view('login');
})->middleware('loginAuth');


Route::get('admin/login','AdminController@login')->middleware('loginAuth');
Route::get('admin/logout', 'AdminController@logout');

Route::post('admin/authenticate', array('as' => 'authenticate', 'uses' => 'AdminController@authlogin'));

//Auth::routes();

Route::get('login', ['as' => 'login', 'uses' => 'AdminController@login']);

Route::group(['middleware'=>['adminAuth'],'prefix' => 'admin'], function() {
	Route::get('dashboard','AdminController@dashboard');

	// All the routes related to users
	Route::get('customers','UsersController@index');
	Route::post('user/deleteuser','UsersController@deleteUser');
	Route::post('user/deleteuseraddress','UsersController@deleteUserAddress');
	Route::post('user/changestatus','UsersController@updateStatus');
	Route::get('customer/edit/{id}','UsersController@edit');
	Route::post('user/storeuser','UsersController@store');

	// All the routes related to pages.
	Route::get('pages','PagesController@index');
	Route::get('addpages','PagesController@addPage');
	Route::post('pages/storepage','PagesController@store');
	Route::post('pages/updatecontent','PagesController@updateContent');
	Route::get('pages/edit/{id}','PagesController@edit');
	Route::get('pages/pagecontent/{id}','PagesController@addContent');
	Route::post('pages/deletepage','PagesController@deletePage');
	Route::post('pages/changestatus','PagesController@updateStatus');
	Route::post('pages/uploadimage','PagesController@uploadImage');


	// All the routes related to hospital service
	Route::get('hospitals','HospitalController@index');
	Route::get('addhospital','HospitalController@addHospital');
	Route::post('hospital/storehospital','HospitalController@store');
	Route::get('hospitals/edit/{id}','HospitalController@edit');
	Route::post('hospital/deletehospital','HospitalController@deleteHospital');
	Route::post('hospital/changestatus','HospitalController@updateStatus');

	// All the routes related to atms service
	Route::get('atms','AtmsController@index');
	Route::get('addatms','AtmsController@addAtms');
	Route::post('atms/storeatms','AtmsController@store');
	Route::post('atms/deleteatm','AtmsController@deleteAtm');
	Route::post('atms/changestatus','AtmsController@updateStatus');
	Route::get('atm/edit/{id}','AtmsController@edit');


	// All the routes related to services.
	Route::get('services','ServiceController@index');
	Route::get('addservice','ServiceController@addService');
	Route::post('services/storeservices','ServiceController@store');
	Route::get('services/edit/{id}','ServiceController@edit');
	Route::post('services/deleteservice','ServiceController@deleteService');
	Route::post('services/changestatus','ServiceController@updateStatus');

	//All the routes related to FAQ
	Route::get('faq','FaqController@index');
	Route::get('addfaq','FaqController@addfaq');
	Route::post('faq/storefaq','FaqController@store');	
	Route::get('faq/edit/{id}','FaqController@edit');
	Route::post('faq/deletefaq','FaqController@deleteFaq');
	Route::post('faq/changestatus','FaqController@changeStatus');

	// All the routes files related to holidays
	Route::get('holidays','HolidaysController@index');
	Route::get('addholiday','HolidaysController@addHoliday');
	Route::post('holidays/storeholiday','HolidaysController@store');
	Route::get('holiday/edit/{id}','HolidaysController@edit');

	// All the routes files related to queries
	
	Route::get('query','QueryController@index');
	Route::post('query/changestatus','QueryController@updateStatus');
        Route::post('query/deletequery','QueryController@deleteQuery');
        Route::get('query/querycontent/{id}','QueryController@viewContent');


	// All the routes related  to reviews
	Route::get('review','ReviewController@index');
	Route::post('review/deletereview','ReviewController@deleteReview');
	Route::post('review/changestatus','ReviewController@updateStatus');

	// All the routes related  to profile
	Route::get('profile','SettingsController@index');
	Route::post('profile/updateinfo','SettingsController@store');

	// All the routes related to travel alerts
	Route::get('travelAlerts','TravelAlertController@index');
	Route::get('addAlerts','TravelAlertController@addAlert');
	Route::post('alert/storeAlerts','TravelAlertController@store');
	Route::get('alert/edit/{id}','TravelAlertController@edit');
	Route::post('alert/deleteAlert','TravelAlertController@deleteAlert');
	Route::post('alert/changeStatus','TravelAlertController@changeStatus');
	
});

Route::get('Admin/users','Users@index');
Route::get('Admin/orders','Orders@index');
Route::get('Admin/invoice','AdminController@invoice');
//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
