<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Holidays extends Model
{
    //
    use SoftDeletes;
    public $table = 'holidays';
    public $primaryKey = 'id';

    protected $fillable = ['title', 'description','holiday_on','status'];
    protected $dates = ['deleted_at'];
}
