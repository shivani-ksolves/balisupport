<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
	use SoftDeletes;
    //
    public $table = 'pages';
    public $primaryKey = 'id';
    protected $fillable = ['name', 'alias', 'keywords','title','status'];
    protected $dates = ['deleted_at'];
}
