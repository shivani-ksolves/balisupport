<?php

namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;


use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;
use App\Query;

class QueryController extends Controller
{
      public function index(){
    	//echo "am working ";
    	$query = Query::get();
    	/*echo "<pre>";
    	print_r($users); 
    	exit ; */
    	return view('query', ['query' => $query]);
    }

     public function updateStatus(){
        $queryId = Input::post('queryId') ;
        $status = Query::where('id',$queryId)->get(['status']);
        
        $status = $status['0']['status'] ; 
        if($status == 0 ){
            $query = Query::find($queryId);
            $query->status = 1 ;
            $query->save();
            echo "activegreen.png";
        }
        else{
            $query = Query::find($queryId);
            $query->status = 0 ; 
            $query->save();
            echo "inactivered.png";
        }
    }

    public function deleteQuery(){
        $queryId = Input::post('queryId');
        $query = Query::find($queryId);
        $query->delete();
    }

    public function viewContent($id){
        
        $query = Query::find($id);
        //print_r($page);
        //exit ; 
        return view('viewquerycontent',['id'=>$id])->with('query',$query);
    }

    // function to insert query data recieved from the API

    public function insertQuery(Request $request){
        $rules = array(
            'first_name'    => 'required',
            'last_name' => 'required',
            'phone'=>'required',
            'email' => 'required',
            'subject'=>'required',
            'query' =>'required',
            );

        /*return response()->json($request->all());
        exit ; */

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $data = array();
            $status = 201 ; 
            $api_message = "failiure";

            return  response()->json(array('data'=>$data,'status'=>$status,'api_message'=>$api_message));
        }else{
            //$insertArray = array('fi');
            $first_name = Input::post('first_name');
            $last_name = Input::post('last_name');
            $email = Input::post('email');
            $phone = Input::post('phone');
            $subject = Input::post('subject'); 
            $query = Input::post('query');
            $status = 0 ; 

            $insertArray = array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'phone'=>$phone,'subject'=>$subject,'query'=>$query,'status'=>$status);



            $query = Query::create($insertArray);
            if($query){
                $data = array();
                $status = 200 ; 
                $api_message = "success";

                return  response()->json(array('data'=>$data,'status'=>$status,'api_message'=>$api_message));
            }else{
                $data = array();
                $status = 202 ; 
                $api_message = "failiure";

                return  response()->json(array('data'=>$data,'status'=>$status,'api_message'=>$api_message));
            }
        }
    }
}
