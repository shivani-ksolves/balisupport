<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Admin;
 
class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
    	echo "am working ";
    }

    public function login(){

        return view('login');
    }


    public function dashboard(){
    	return view('dashboard');
    }

    public function invoice(){
    	return view('invoice');
    }
    public function authlogin(Request $request) {

        //r_id = $request->input('user_id');
       $rules = array(
    		'email'    => 'required|email', // make sure the username field is not empty
    		'password' => 'required|alphaNum' // password can only be alphanumeric and has to be greater than 3 characters
    	);

    	// validate post data
    	$validator = Validator::make(Input::all(), $rules);

    	// Check validator status
    	if ($validator->fails()) {
    		return Redirect::to('/admin/login')
    			->withErrors($validator) // send back all errors to the login form
    			->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    	}
        else {
            Auth::shouldUse('admin');
            $userdata = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
            );

            if(Auth::attempt($userdata)){
                $user = Auth::user();
                Session::put('name', $user['first_name']." ".$user['last_name']);
                Session::put('id', $user['id']);
               return Redirect::to('/admin/dashboard');

            }else{
                return Redirect::to('/admin/login')->withErrors('Incorrect login details');
            }
        }
    }

    public function logout(Request $request){
        Auth::shouldUse('admin');

         Auth::logout();
         return Redirect::to('/admin/login');
    }   
}
