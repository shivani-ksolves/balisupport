<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Settings;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\ImageHelper;
// class PagesController to controll all the functionality related to pages.
class SettingsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
        
        $settings = Settings::find(1);
        /*echo "<pre>";
        print_r($settings) ; 
        exit ; */
        return view('editsetting', ['settings' => $settings]);

    }

    

    public function store(Request $request){
        
        $serviceId = 1 ;  //Input::post('serviceId');
        $office_address = Input::post('office_address');
        $phone = Input::post('phone');
        $email = Input::post('email'); 
        $office_longitude = Input::post('office_longitude');
        $office_latitude = Input::post('office_latitude');
        

        $rules = array(
                'office_address'    => 'required',
                'phone' => 'required',
                'email' => 'required',
                'office_longitude' => 'required',
                'office_latitude' => 'required',
                );
        

        
        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {
            //$insertArray = array('office_address'=>$office_address, 'email'=>$email,'phone'=>$phone,'office_longitude'=>$office_longitude,'office_latitude'=>$office_latitude); 

                $settings = Settings::find('1');
                $settings->office_address = $office_address ; 
                $settings->email = $email ; 
                $settings->phone = $phone ; 
                $settings->office_longitude = $office_longitude ; 
                $settings->office_latitude = $office_latitude ;
                $settings->save();
                //set the success mesasge
                Session::flash('success', 'Profile information has been udpated');
                return redirect('admin/profile');
            
        }
        

    }

    // API Function for getting the contact - us information

    public function getContactUsInfo(){
        $settings = Settings::find(1);
        
        return response()->json($settings);    
    }


}
