<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\ImageHelper;
use Config;

class ServiceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
        
        $services = Services::get();
        return view('services', ['services' => $services]);

    }

    // API Function for getting the services

    public function getServices(){
        $services = Services::get();
        for($i=0 ; $i < count($services);$i++){
            $services[$i]['image_name'] = ImageHelper::getImage($services[$i]['image_name'],'service');
        }
        return response()->json(array('data'=>$services));    
    }



    public function addService(){
    	//echo "this is add page";
    	return view('addservice');
    }


    public function edit($id){
    // get the page data of a particular id.
    $service = Services::find($id);
    // show the edit form and pass the page-data
    return view('addservice')->with('service', $service);
    }

    public function store(Request $request){
        
        $serviceId = Input::post('serviceId');
        $old_image = Input::post('old_image');
        $old_title = Input::post('old_title');
        $title = Input::post('title'); 
        if($serviceId != '' && $old_image != ''  ){

            if($old_title != $title  ){
                $rules = array(
                'title'    => 'required|unique:services,title,NULL,id,deleted_at,NULL',
                'description' => 'required',
                'is_paid' => 'required',);
            }else{
                $rules = array(
                'title'    => 'required',
                'description' => 'required',
                'is_paid' => 'required',);
            }
        }
        else{
            $rules = array(
            'title'    => 'required|unique:services,title,NULL,id,deleted_at,NULL',
            'description' => 'required',
            'is_paid' => 'required',
            'service_image' => 'required',
            );

        }
        

        
        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {
            $title = Input::post('title');
            $description = Input::post('description') ; 
            $is_paid = Input::post('is_paid') ;

            if($serviceId != '' ){

                // create to query to update the data
                $file = $request->file('service_image');
                
                $services = Services::find($serviceId);
                $services->title = $title ; 
                $services->description = $description ; 
                $services->is_paid = $is_paid  ; 
                if(isset($file)){
                    
                    $destinationPath = public_path('uploads/service');
                    $file = $request->file('service_image');
                    $image_name = str_replace(' ', '', $title).time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$image_name);
                    $services->image_name = $image_name ; 

                if(file_exists(public_path('uploads/service/'.$old_image)) AND !empty($old_image)){
                unlink(public_path('uploads/service/'.$old_image));
                }



                }
                $services->save();
                // settig  the sucess message 
                Session::flash('success', 'Service information has been udpated');
                // passing the data to pages view.
                //return view('pages', ['pages' => $pages]);
                return redirect('admin/services');

            }
            else{

                $destinationPath = public_path('uploads/service');
                $file = $request->file('service_image');
                $image_name = str_replace(' ', '', $title).time().'.'.$file->getClientOriginalExtension();
                $file->move($destinationPath,$image_name);
                // create to query to save the data
                $insertArray = array('title'=>$title, 'description'=>$description,'is_paid'=>$is_paid,'image_name'=>$image_name); 

                $services = Services::create($insertArray);
                //set the success mesasge
                Session::flash('success', 'Service information has been added');
                return redirect('admin/services');
            }
            
        }
        

    }


    public function deleteService(){
        $serviceId = Input::post('serviceId');
        $services = Services::find($serviceId);
        $services->delete();
    }


    public function updateStatus(){
        $serviceId = Input::post('serviceId') ;
        $status = Services::where('id',$serviceId)->get(['status']);
        
        $status = $status['0']['status'] ; 
        if($status == 0 ){
            $services = Services::find($serviceId);
            $services->status = '1' ;
            $services->save();
            echo "activegreen.png";
        }
        else{
            $services = Services::find($serviceId);
            $services->status = '0' ; 
            $services->save();
            echo "inactivered.png";
        }
    }

    /* Function to get social media links */
    public function getSocialLinks(){
        $social_media_links = config('app.social_media_links');
        return response()->json($social_media_links);
    }
}
