<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\TravelAlert;

class TravelAlertController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
    	$alerts = TravelAlert::get();
    	return view('travelAlert', ['alerts' => $alerts]);
    }

    public function addAlert(){
        return view('addAlert');
    }

    public function edit($id){
        $alerts = TravelAlert::find($id);
        return view('addAlert', ['alert' => $alerts]);
    }

    /* Function to add/edit alert */
    public function store(Request $request){
        
        $rules = array(
            'title'    => 'required',
            'description' => 'required',
            'start_date'=>'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {
            $alertId = Input::post('alertId');

            if($alertId != '' ){

                $travelAlert = TravelAlert::find($alertId);
                $travelAlert->title = Input::post('title');
                $travelAlert->description = Input::post('description'); 
                $travelAlert->start_date = Input::post('start_date'); 
                $travelAlert->end_date = Input::post('end_date'); 
                $travelAlert->save();
            
                Session::flash('success', 'Alert udpated successfully'); 
                return redirect('admin/travelAlerts');             
            }
            else{

                $save = TravelAlert::create($request->all());                           
                Session::flash('success', 'Alert added successfully');                
                return redirect('admin/travelAlerts');             
            }
        }
    }

    /* Function to delet alert */
    public function deleteAlert(){
        $alertId = Input::post('alertId');
        $alert = TravelAlert::find($alertId);

        if($alert->delete()){
           return 'true'; 
        }
        return 'false';
    }

    /* Function to get Alert for mobile api */
    public function getAlert(){
        $start_date = date('Y-m-d', strtotime('-7 days'));
        $end_date = date('Y-m-d', strtotime('+7 days'));
        $alert = TravelAlert::where('start_date','>=',$start_date)->where('start_date','<=',$end_date)->where('status',1)->get();

        if(isset($alert[0])) {
            $data['data'] = $alert;
            return response()->json($data);
        }
        else {
            $data['data'] = $alert;
            $data['message'] = "No Data Found";
            return response()->json($data);
        }
        
    }

    /* Function to change status of alert */
    public function changeStatus(){
        
        $alertId = Input::post('alertId') ;
        $status = TravelAlert::where('id',$alertId)->get(['status']);
        $status = $status['0']['status'] ;
        $alert = TravelAlert::find($alertId); 
        if($status == 0 ){
            $alert->status = 1 ;
            $img = "activegreen.png";
        }
        else{
            $alert->status = 0 ; 
            $img = "inactivered.png";
        }
        $alert->save();
        echo $img;
    }
}
