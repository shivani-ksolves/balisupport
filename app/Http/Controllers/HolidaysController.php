<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Holidays;

class HolidaysController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
		$holidays = Holidays::get();
		//$holiday = array();
		foreach($holidays as $holidays){
			$holiday[] = array('title'=>$holidays['title'],'start'=>$holidays['holiday_on'],'url'=>'holiday/edit/'.$holidays['id']);
            $holidays = json_encode($holiday) ;
		}
		
		
    	return view('calendar', ['holidays' => $holidays]);
    }

    public function addHoliday(){
    	return view('addholidays');
    }


    public function edit($id){
    // get the page data of a particular id.
    $holiday = Holidays::find($id);
    // show the edit form and pass the page-data
    return view('addholidays')->with('holiday', $holiday);
    }

    public function store(Request $request){

        $rules = array(
            'title'    => 'required',
            'description' => 'required',
            'holiday_on'=>'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else{
            $holidayId = Input::post('holidayId');
            if($holidayId != '' ){

                // create to query to update the data
                $holiday = Holidays::find($holidayId);
                $holiday->title = Input::post('title'); //'Test';
                $holiday->holiday_on = Input::post('holiday_on'); //'address';
                $holiday->description = Input::post('description');
                $holiday->save();
                
                // settig  the sucess message 
                Session::flash('success', 'Holiday information has been udpated');
                
                return redirect('admin/holidays');

            }
            else{
                // query to save the hospital data.
                $postData = Input::post();
                 
               /* print_r($postData) ; 
                exit ; */
                $holidays = Holidays::create($request->all());
                
                //set the success mesasge
                Session::flash('success', 'Holiday information has been added');
                //return view('hospitals', ['hospitals' => $hospitals]);

                return redirect('admin/holidays');

            }
        }

    }

    public function getHolidays(){

        $holidays = Holidays::where('status',1)->get();

        if(isset($holidays[0])) {
            $data['data'] = $holidays;
            $data['status'] = '200';
            $data['api_message'] = 'success';
            return response()->json($data);
        }
        else {
            $data['data'] = $holidays;
            $data['status'] = '200';
            $data['api_message'] = 'No data found';
            return response()->json($data);
        }
    }
}
