<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use App\Atms;

class AtmsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$atms = Atms::get();
        return view('atms', ['atms' => $atms]);
    }

    public function addAtms(){
    	//echo "this is add page";
    	return view('addatms');
    }
    public function store(Request $request){

        $rules = array(
            'bankname'    => 'required',
            'address' => 'required',
            'phone'=>'required',
            'lat' => 'required',//['','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'], 
            'long' =>'required', //['','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else{
            $atmId = Input::post('atmId');
            if($atmId != '' ){

                // create to query to update the data
                $atms = Atms::find($atmId);
                $atms->bankname = Input::post('bankname'); //'Test';
                $atms->address = Input::post('address'); //'address';
                $atms->phone = Input::post('phone'); //'address';
                $atms->long = Input::post('long'); //'longitude';
                $atms->lat = Input::post('lat'); //'latitude';
                $atms->type = Input::post('type'); //'set type 1-ATM, 2-Bank';
                $atms->save();
                
                // settig  the sucess message 
                Session::flash('success', 'Information has been udpated');
                
                return redirect('admin/atms');

            }
            else{
                // query to save the hospital data.
                $atms = Atms::create($request->all());
                
                //set the success mesasge
                Session::flash('success', 'Information has been added');
                //return view('hospitals', ['hospitals' => $hospitals]);

                return redirect('admin/atms');

            }
        }

    }

    public function edit($id){
    // get the page data of a particular id.
    $atm = Atms::find($id);
    // show the edit form and pass the page-data
    return view('addatms')->with('atm', $atm);
    //print_r($hospitals);
    }


    public function deleteAtm(){
        $atmId = Input::Post('atmId');
        $atms = Atms::find($atmId);
        $atms->delete();
    }

    public function updateStatus(){
        $atmId = Input::Post('atmId') ;
        $status = Atms::where('id',$atmId)->get(['status']);
        
        $status = $status['0']['status'] ;  
        if($status == 0 ){
            $atms = Atms::find($atmId);
            $atms->status = '1' ;
            $atms->save();
            echo "activegreen.png";
        }
        else{
            $atms = Atms::find($atmId);
            $atms->status = '0' ; 
            $atms->save();
            echo "inactivered.png";
        }
    }

    /* Function to get ATMs and Banks information for mobile api */
    public function getAtmInfo(){

        $api_name = Route::currentRouteName();
        if($api_name == 'atm_api')
            $atms = Atms::where('status',1)->where('type',1)->get();
        else
            $atms = Atms::where('status',1)->where('type',2)->get();

        if(isset($atms[0])) {
            $data['data'] = $atms;
            return response()->json($data);
        }
        else {
            $data['data'] = $atms;
            $data['api_message'] = "No Data Found";
            $data['status'] = 200;
            return response()->json($data);
        }
    }
}
