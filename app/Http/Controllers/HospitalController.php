<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Hospitals;

class HospitalController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$hospitals = Hospitals::get();
    	return view('hospitals', ['hospitals' => $hospitals]);
    }

    // for Api 

    public function getHospitals(){
        $hospitals = Hospitals::get();
        return response()->json($hospitals);
    }

    public function addHospital(){
    	//echo "this is add page";
    	return view('addhospital');
    }
    public function store(Request $request){

        $rules = array(
            'name'    => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'speciality'=>'required',
            'lat' => 'required', //['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'], 
            'long' => 'required', //['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else{
            $hospitalId = Input::post('hospitalId');
            if($hospitalId != '' ){

                // create to query to update the data
                $hospitals = Hospitals::find(Input::post('hospitalId'));
                $hospitals->name = Input::post('name'); //'Test';
                $hospitals->email = Input::post('email'); //'Test@foo.com';
                $hospitals->phone = Input::post('phone'); //'+62-111-111-111';
                $hospitals->address = Input::post('address'); //'address';
                $hospitals->speciality = Input::post('speciality'); //'speciality of hospital';
                $hospitals->long = Input::post('long'); //'longitude';
                $hospitals->lat = Input::post('lat'); //'latitude';
                $hospitals->save();
                // query to get the data to show on the pages view.
                $hospitals = Hospitals::get();

                // settig  the sucess message 
                Session::flash('success', 'Hospital information has been udpated');
                // passing the data to pages view.
                //return view('hospitals', ['hospitals' => $hospitals]);

                return redirect('admin/hospitals');

            }
            else{
                // query to save the hospital data.
                $hospitals = Hospitals::create($request->all());
                // query to get all the information of a particular hospital.
                $hospitals = Hospitals::get();
                //set the success mesasge
                Session::flash('success', 'Hospital information has been added');
                //return view('hospitals', ['hospitals' => $hospitals]);

                return redirect('admin/hospitals');

            }
        }

    }

    public function edit($id){
    // get the page data of a particular id.
    $hospitals = Hospitals::find($id);
    // show the edit form and pass the page-data
    return view('addhospital')->with('hospitals', $hospitals);
    //print_r($hospitals);
    }


    public function deleteHospital(){
        $hospitalId = Input::Post('hospitalId');
        $hospital = Hospitals::find($hospitalId);
        $hospital->delete();
    }

    public function updateStatus(){
        $hospitalId = Input::Post('hospitalId') ;
        $status = Hospitals::where('id',$hospitalId)->get(['status']);
        
        $status = $status['0']['status'] ;  
        if($status == 0 ){
            $hospitals = Hospitals::find($hospitalId);
            $hospitals->status = '1' ;
            $hospitals->save();
            echo "activegreen.png";
        }
        else{
            $hospitals = Hospitals::find($hospitalId);
            $hospitals->status = '0' ; 
            $hospitals->save();
            echo "inactivered.png";
        }
    }


}
