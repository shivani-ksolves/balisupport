<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Pages;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\ImageHelper;

// class PagesController to controll all the functionality related to pages.
class PagesController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$pages = Pages::get();
    	return view('pages', ['pages' => $pages]);
    }

    public function addPage(){
    	//echo "this is add page";
    	return view('addpages');
    }


    public function edit($id){
    // get the page data of a particular id.
    $page = Pages::find($id);
    // show the edit form and pass the page-data
    return view('addpages')->with('page', $page);
    }

    public function addContent($id){
        
        $page = Pages::find($id);
        //print_r($page);
        //exit ; 
        return view('addpagecontent',['id'=>$id])->with('page',$page);
    }



    public function store(Request $request){
        $pageId = Input::post('pageId');
        $oldAlias = Input::post('oldAlias');
        $alias = Input::post('alias');

        if($pageId != '' ){
            if($oldAlias != $alias ){
                $rules = array(
            'name'    => 'required',
            'alias' => 'required|unique:pages,alias,NULL,id,deleted_at,NULL|regex:/(^[A-Za-z0-9-]+$)+/',
            'keywords' => 'required',
            'title' => 'required',
            );
            }else{
                $rules = array(
            'name'    => 'required',
            'alias' => 'required|regex:/(^[A-Za-z0-9 -]+$)+/',
            'keywords' => 'required',
            'title' => 'required',
            );
            }
        }
        else{

            $rules = array(
            'name'    => 'required',
            'alias' => 'required|unique:pages,alias,NULL,id,deleted_at,NULL|regex:/(^[A-Za-z0-9-]+$)+/',
            'keywords' => 'required',
            'title' => 'required',
            );


        }
        
        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {

            if($pageId != '' ){

                // create to query to update the data
                $pages = Pages::find($pageId);
                $pages->name = Input::post('name'); //'john@foo.com';
                $pages->alias = Input::post('alias'); //'john@foo.com';
                $pages->keywords = Input::post('keywords'); //'john@foo.com';
                $pages->title = Input::post('title'); //'john@foo.com';
                $pages->save();
                // query to get the data to show on the pages view.
                $pages = Pages::get();
                // settig  the sucess message 
                Session::flash('success', 'Page information has been udpated');
                // passing the data to pages view.
                //return view('pages', ['pages' => $pages]);
                return redirect('admin/pages');

            }
            else{
                // create to query to save the data
                $pages = Pages::create($request->all());
                // query to get the data to show on the pages view.
                $pages = Pages::get();
                //set the success mesasge
                Session::flash('success', 'Page information has been added');
                // passing the data to pages view.
                //return view('pages', ['pages' => $pages]);

                return redirect('admin/pages');
            }
            
        }
        

    }


    public function uploadImage(Request $request){

        $file = $request->file('image_to_upload');
        //print_r($file) ; 

        $image_name = time().'.'.$file->getClientOriginalExtension();
        //echo $image_name ; 
        //exit ; 
        $destinationPath = 'uploads/cms';
        $file->move($destinationPath,$image_name);

        $Image = array('imageSrc'=>ImageHelper::getImage($image_name,'cms'));
        echo $Image['imageSrc'] ;  //json_encode($Image) ;
    }

    

    public function updateContent(){
        
        $rules = array(
            'pageContent'    => 'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
           //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {
            // create to query to save the data
            $pageId = Input::post('pageId') ;
            $pages = Pages::find($pageId);
            $pages->content = Input::post('pageContent'); //'john@foo.com';
            $pages->save();
            // query to get the data to show on the pages view.
            $pages = Pages::get();
            Session::flash('success', 'Page content has been updated');
            // passing the data to pages view.
            //return view('pages', ['pages' => $pages]);
            return redirect('admin/pages');
        }
    }


    public function deletePage(){
        $pageId = Input::post('pageId');
        $pages = Pages::find($pageId);
        $pages->delete();
    }


    public function updateStatus(){
        $pageId = Input::post('pageId') ;
        $status = Pages::where('id',$pageId)->get(['status']);
        
        $status = $status['0']['status'] ; 
        if($status == 0 ){
            $pages = Pages::find($pageId);
            $pages->status = 1 ;
            $pages->save();
            echo "activegreen.png";
        }
        else{
            $pages = Pages::find($pageId);
            $pages->status = 0 ; 
            $pages->save();
            echo "inactivered.png";
        }
    }
}
