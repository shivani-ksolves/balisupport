<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Faq;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

// class FaqController to controll all the functionality related to pages.
class FaqController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$faq = Faq::get();
        return view('faq', ['faq' => $faq]);
    }

    public function addFaq(){
    	//echo "this is add page";
    	return view('addfaq');
    }

    public function edit($id){
        $faq = Faq::find($id);
        return view('addfaq')->with('faq', $faq);
    }

    public function store(Request $request){
        
        $rules = array(
            'question'    => 'required',
            'answer' => 'required',
         );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else {
            $faqId = Input::post('faqId');

            if($faqId != '' ){

                $faq = Faq::find($faqId);
                $faq->question = Input::post('question');
                $faq->answer = Input::post('answer'); 
                $faq->save();
                
                $faq = Faq::get();   
                Session::flash('success', 'FAQ udpated successfully'); 
                return redirect('admin/faq');             
                //return view('faq', ['faq' => $faq]);

            }
            else{

                $save = Faq::create($request->all());                
                $faq = Faq::get();                
                Session::flash('success', 'Question added successfully');                
                //return view('faq', ['faq' => $faq]);
                return redirect('admin/faq');             
            }
        }
    }

    public function deleteFaq(){
        $faqId = Input::post('faqId');
        $faq = Faq::find($faqId);

        if($faq->delete()){
           return 'true'; 
        }
        return 'false';
    }

    public function changeStatus(){
        
        $faqId = Input::post('faqId') ;
        $status = Faq::where('id',$faqId)->get(['status']);
        $status = $status['0']['status'] ;
        $faq = Faq::find($faqId); 
        if($status == 0 ){
            $faq->status = 1 ;
            $img = "activegreen.png";
        }
        else{
            $faq->status = 0 ; 
            $img = "inactivered.png";
        }
        $faq->save();
        echo $img;
    }

    /* Function to get FAQ for mobile api */
    public function getFaq(){

        $faq = Faq::where('status',1)->get();

        if(isset($faq[0])) {
            $data['data'] = $faq;
            return response()->json($data);
        }
        else {
            $data['data'] = $faq;
            $data['message'] = "No Data Found";
            return response()->json($data);
        }
        
    }
}
