<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;


use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Redirect;


use App\User as Users;

use App\UsersAddress;
use Illuminate\Support\Facades\Auth;

class UsersController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){

    	$users = Users::get();
    	return view('users', ['users' => $users]);
    }

    public function login(Request $request){


        $rules = array(
            'email'    => 'required',
            'password' => 'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);
         if ($validator->fails()) {
            $response = array('data'=>array(),'status'=>202,'api_msg'=>"Please provide all information");
            //print_r($_POST);
            return response()->json(array($response)); 
         }else{

            $credentials = array(
            'email'     => $request->input('email'),
            'password'  => $request->input('password')
            );
            
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
                $success['token'] =  $user->createToken('MyApp')->accessToken;
                return response()->json(['success' => $success]);
            }
            else{
                $response = array('data'=>array(),'status'=>202,'api_msg'=>"Email or password is invalid");
                //print_r($_POST);
                return response()->json(array($response)); 
            }

         }


        

    }

    public function edit($userId){

         // get the page data of a particular id.
        $users = Users::find($userId);

        // get the users address from user address table

        $user_address = UsersAddress::where('user_id',$userId)->get();
        // show the edit form and pass the page-data
        return view('edituser', compact('users','user_address'));
    }


    public function store(){
        $userId = Input::Post('userId');
        $rules = array(
            'first_name'    => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'state_id.*' => 'required',
            'city_id.*' => 'required',
            'address.*' => 'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            return Redirect::back()->withInput()->withErrors($validator);
        }else{
            // create to query to update the data
            $users = Users::find($userId);
            $users->first_name = Input::post('first_name'); //'john@foo.com';
            $users->last_name = Input::post('last_name'); //'john@foo.com';
            $users->phone = Input::post('phone'); //'john@foo.com';
            $users->gender = Input::post('gender'); //'john@foo.com';
            $users->save();


            $states = Input::post('state_id');
            $cities = Input::post('city_id');
            $address = Input::post('address');
            $address_id = Input::post('address_id');
            for($i = 0 ; $i < count($states) ;$i++ ){
                if(isset($address_id[$i]) && $address_id[$i] != ''  ){
                    $users_address = UsersAddress::find($address_id[$i]);
                    $users_address->country_id = 1; 
                    $users_address->state_id = $states[$i]; 
                    $users_address->city_id = $cities[$i];
                    $users_address->address = $address[$i];
                    $users_address->save();
                }else{

                    UsersAddress::create(['user_id' =>$userId,'country_id' =>1,'state_id'=>$states[$i],'city_id'=>$cities[$i],'address' =>$address[$i]]);

                }
            }
            // settig  the sucess message 
            Session::flash('success', 'Customer information has been udpated');
            // passing the data to pages view.
            //-return view('users', ['users' => $users]);
            return redirect('admin/customers');


        }


    }



    public function deleteUser(){
        $userId = Input::post('userId');
        $users = Users::find($userId);
        $users->delete();
    }

    public function deleteUserAddress(){
        $addressId = Input::post('addressId');
        $UsersAddress = UsersAddress::find($addressId);
        $UsersAddress->delete();   
    }


    public function updateStatus(){
        $userId = Input::post('userId') ;
        $status = Users::where('id',$userId)->get(['status']);
        
        $status = $status['0']['status'] ; 
        if($status == 0 ){
            $users = Users::find($userId);
            $users->status = 1 ;
            $users->save();
            echo "activegreen.png";
        }
        else{
            $users = Users::find($userId);
            $users->status = 0 ; 
            $users->save();
            echo "inactivered.png";
        }
    }



    // API Functions 


    public function userSignup(){


        $rules = array(
            'first_name'    => 'required',
            'last_name' => 'required',
            'username'=>'required|unique:users,username',
            'email'=>'required|unique:users,email',
            'phone' => 'required',
            'password'=>'required',
            'gender'=>'required'
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);

        // Check validator status
        if ($validator->fails()) {
            //echo "validation failed";
            $failedRules = $validator->failed();
            if(isset($failedRules['email']['Unique'])){

            $response = array('data'=>array(),'status'=>202,'api_msg'=>"Email already in use");
            return response()->json(array($response));  


            }
            else if(isset($failedRules['username']['Unique'])){

            $response = array('data'=>array(),'status'=>202,'api_msg'=>"Username already taken");
            return response()->json(array($response));  

            }
            else{

            $response = array('data'=>array(),'status'=>202,'api_msg'=>"Please provide all information");
            return response()->json(array($response)); 
            
            }
        }else{

            $first_name = Input::post('first_name');
            $last_name = Input::post('last_name');
            $username = Input::post('username');
            $email = Input::post('email');
            $password = bcrypt(Input::post('password'));
            $phone = Input::post('phone');
            $gender = Input::post('gender');

            $insertArray = array('first_name'=>$first_name,'last_name'=>$last_name,'username'=>$username,'email'=>$email,'password'=>$password,'phone'=>$phone,'gender'=>$gender,'status'=>1);

            $users = Users::create($insertArray);
            if($users){

                $response = array('data'=>array(),'status'=>200,'api_msg'=>"Registration successfull");
                return response()->json(array($response));

            }else{

                $response = array('data'=>array(),'status'=>202,'api_msg'=>"Internal server error");
                 return response()->json(array($response)); 
            }


        }
    }


    public function userLogin(){
        $rules = array(
            'email'    => 'required',
            'password' => 'required',
            );

        // validate post data
        $validator = Validator::make(Input::all(), $rules);
         if ($validator->fails()) {
            $response = array('data'=>array(),'status'=>202,'api_msg'=>"Please provide all information");
            print_r($_POST);
            //return response()->json(array($response)); 
         }else{

         }
    }
}
