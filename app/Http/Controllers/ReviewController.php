<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\ImageHelper;

use App\Reviews;

// class PagesController to controll all the functionality related to pages.
class ReviewController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(){
    	$reviews = Reviews::getData();
        /*echo "<pre>";
        print_r($reviews);
        exit ; */
    	return view('reviews', ['reviews' => $reviews]);
    }

    

    public function deleteReview(){
        $reviewId = Input::post('reviewId');
        $reviews = Reviews::find($reviewId);
        $reviews->delete();
    }


    public function updateStatus(){
        $reviewId = Input::post('reviewId') ;
        $status = Reviews::where('id',$reviewId)->get(['status']);
        
        $status = $status['0']['status'] ; 
        if($status == 0 ){
            $reviews = Reviews::find($reviewId);
            $reviews->status = 1 ;
            $reviews->save();
            echo "activegreen.png";
        }
        else{
            $reviews = Reviews::find($reviewId);
            $reviews->status = 0 ; 
            $reviews->save();
            echo "inactivered.png";
        }
    }
}
