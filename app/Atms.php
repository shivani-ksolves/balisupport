<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Atms extends Model
{
    //
    use SoftDeletes;
    //
    public $table = 'atms';
    public $primaryKey = 'id';
    protected $fillable = ['bankname', 'address', 'long','lat','status','type','phone'];
    protected $dates = ['deleted_at'];
}
