<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

	protected $guard = 'api';
    /*	protected $fillable = [
        'name', 'email', 'password',
    ];
*/
    use SoftDeletes;
    //
    public $table = 'users';
    //public $primaryKey = 'id';
    public $primaryKey = 'id';
    protected $fillable = ['first_name', 'last_name','email','phone','username','gender','password','status'];
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function review(){
        return $this->belongsTo('App\Reviews','user_id','id');
    }
}
