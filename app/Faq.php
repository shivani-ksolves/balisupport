<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
	use SoftDeletes;

    public $table = 'faq';
    public $primaryKey = 'id';
    protected $fillable = ['question', 'answer','status','created_by'];
    protected $dates = ['deleted_at'];
}
