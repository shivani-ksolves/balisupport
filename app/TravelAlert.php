<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TravelAlert extends Model
{
	use SoftDeletes;

	public $primaryKey = 'id';
    protected $fillable = ['title',
        'description', 'start_date', 'end_date',
    ];
    protected $dates = ['deleted_at'];

    public function getEndDateAttribute($value)
	{
		if($value == null){
			$value = '';
		}
	    return $value;
	}

}
