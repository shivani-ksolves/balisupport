<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{
    //
    use SoftDeletes;
    public $table = 'reviews';
    public $primaryKey = 'id';

    protected $fillable = ['user_id', 'rating', 'review','status'];
    protected $dates = ['deleted_at'];

    public function user()
    {
        
        return $this->hasOne('App\User','id', 'user_id');
    }
    public static function getData(){
    	return Reviews::with('user')->get();
    }
}
