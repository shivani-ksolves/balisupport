<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
	use SoftDeletes, HasApiTokens;
    
    public $table = 'users';

    public $primaryKey = 'id';
    protected $fillable = ['name', 'first_name', 'last_name','phone','gender','status'];
    protected $dates = ['deleted_at'];
    protected $hidden = [

        'password', 'remember_token',

    ];
    public function review(){
    	return $this->belongsTo('App\Reviews','user_id','id');
    }
}
