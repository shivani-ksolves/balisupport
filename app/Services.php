<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    //
    use SoftDeletes;
    //
    public $table = 'services';
    public $primaryKey = 'id';
    protected $fillable = ['title', 'description', 'image_name','status','is_paid'];
    protected $dates = ['deleted_at'];
}
