<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Settings extends Model
{
    //
    use SoftDeletes;
    //
    public $table = 'settings';
    public $primaryKey = 'id';
    protected $fillable = ['office_address', 'email', 'phone','office_latitude','office_longitude'];
    protected $dates = ['deleted_at'];
}
