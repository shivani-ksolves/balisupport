<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersAddress extends Model
{
	use SoftDeletes;
    public $table = 'users_address';
    //public $primaryKey = 'id';
    public $primaryKey = 'id';
    protected $fillable = ['user_id', 'country_id', 'state_id','city_id','address'];
    protected $dates = ['deleted_at'];
}
