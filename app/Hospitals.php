<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospitals extends Model
{
    //
    //
    use SoftDeletes;
    public $table = 'hospitals';
    public $primaryKey = 'id';

    protected $fillable = ['name', 'address', 'email','phone','long','lat','speciality','status'];
    protected $dates = ['deleted_at'];
}
