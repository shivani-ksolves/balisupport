<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Query extends Model
{
    use SoftDeletes;
    //
    public $table = 'query';
    //public $primaryKey = 'id';
    public $primaryKey = 'id';
    protected $fillable = ['first_name', 'last_name','email','phone','subject','query','status'];
    protected $dates = ['deleted_at'];
}
