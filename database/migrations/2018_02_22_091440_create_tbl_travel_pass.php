<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTravelPass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_pass', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50)->nullable();
            $table->tinyInteger('discount_type')->comment('0-percentage,1-dollar');
            $table->integer('discount_value');
            $table->tinyInteger('is_active')->comment('0-inactive,1-active');
            $table->string('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_pass');
    }
}
