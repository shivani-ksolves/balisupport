
            $table->string('title',50)->nullable();
            $table->string('description',140)->nullable();<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',50)->nullable();
            $table->text('description')->nullable();
            $table->string('image_name',140)->nullable();
            $table->tinyInteger('is_paid')->default(0)->comment('0-not_paid,1-paid');
            $table->tinyInteger('status')->default(1)->comment('0-inactive,1-active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
