<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name',50)->nullable();
            $table->string('alias',50)->nullable();
            $table->string('url',50)->nullable();
            $table->string('title',50)->nullable();
            $table->string('keywords',140)->nullable();
            $table->text('content')->nullable();
            
            $table->tinyInteger('is_default')->default(0)->comment('0-no,1-yes');
            $table->tinyInteger('status')->default(1)->comment('0-inactive,1-active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
