<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50)->nullable();
            $table->string('address',140)->nullable();
            $table->string('email')->nullable();
            $table->string('phone',20)->nullable();
            
            $table->string('speciality',140)->nullable();
            $table->tinyInteger('status')->default(1)->comment('0-inactive,1-active');
            $table->string('lat',30)->nullable();
            $table->string('long',30)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitals');
    }
}
