<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
        
        [   'first_name' => 'John',
            'last_name' => 'Doe',
            'username' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'gender' => 1,
            'phone' => str_random(10),
            'status' => 1,
        ],
        [   'first_name' => 'John',
            'last_name' => 'Doe',
            'username' => str_random(10),
            'email' => str_random(10).'@gmail.com',
            'password' => bcrypt('secret'),
            'gender' => 1,
            'phone' => str_random(10),
            'status' => 1,
        ],
        
    ]
    );
    }
}
