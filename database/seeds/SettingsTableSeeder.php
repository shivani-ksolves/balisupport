<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('settings')->insert(
        
        [   'office_address' => 'Customer support ,bali',
        	'phone' => 'Doe', 
        	'email' => 'admin@gmail.com', 
        	'office_longitude' => '123456', 
        	'office_latitude' => '45698',
        ]);
    }
}
