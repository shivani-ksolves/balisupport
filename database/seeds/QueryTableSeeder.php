<?php

use Illuminate\Database\Seeder;

class QueryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	 DB::table('query')->insert([
	        
	        [   
	        	'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "hhh",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "jdfhdjddj",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "pppp",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "kkkk",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "wwwww",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "oooo",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "lssdjd",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [  'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "ksj",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "sbbxx",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ],
	        [   'first_name' => 'John',
	            'last_name' => 'Doe',
	            'email' => str_random(10).'@gmail.com',
	            'subject' => "aaa",
	            'query'=>"jhsghdgsahvdhsahabh  jkashxjhbsxbjsb",
	            'status' => 1,
	            'phone' => str_random(10),
	        ]
	    ]);
    }
}
