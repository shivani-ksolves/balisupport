@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Atm/Bank</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/atms/storeatms')}}" method="POST" class="form-horizontal">

            
            {{ csrf_field() }}
            <input type="hidden" name="atmId" value="@isset($atm->id){{$atm->id}} @endisset">           

              <div class="box-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <select  class="form-control" name="type">
                      <option value="1" @if(isset($atm->type) && $atm->type == 1) selected @endif>ATM</option>
                      <option value="2" @if(isset($atm->type) && $atm->type == 2) selected @endif>Bank</option>
                    </select> 
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Bank Name *</label>

                  <div class="col-sm-10">
                    <input type="text" maxlength="50" required name="bankname" class="form-control" id="inputEmail3" placeholder="Bank Name" value="@if(old('bankname') != '' ){{old('bankname')}} @elseif(isset($atm->bankname) && old('bankname') == ''){{$atm->bankname}} @endif">
                    <p style="color:red;">{{ $errors->first('bankname') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Phone No. *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="phone" class="form-control" placeholder="Phone No" value="@if(old('phone') != '' ){{old('phone')}} @elseif(isset($atm->phone) && old('phone') == ''){{$atm->phone}} @endif">
                    <p style="color:red;">{{ $errors->first('phone') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Address *</label>

                  <div class="col-sm-10">
                    <input type="text" maxlength="50" required name="address" class="form-control" id="inputPassword3" placeholder="Address" value="@if(old('address') != '' ){{old('address')}} @elseif(isset($atm->address) && old('address') == ''){{$atm->address}} @endif">
                    <p style="color:red;">{{ $errors->first('address') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Longitude *</label>

                  <div class="col-sm-10">
                    <input type="" required name="long" class="form-control" id="inputPassword3" placeholder="Longitude" value="@if(old('long') != '' ){{old('long')}} @elseif(isset($atm->long) && old('long') == ''){{$atm->long}} @endif">
                    <p style="color:red;">{{ $errors->first('long') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Latitude *</label>

                  <div class="col-sm-10">
                    <input type="" required name="lat" class="form-control" id="inputPassword3" placeholder="Latitude" value="@if(old('lat') != '' ){{old('lat')}} @elseif(isset($atm->lat) && old('lat') == ''){{$atm->lat}} @endif">
                    <p style="color:red;">{{ $errors->first('lat') }}</p>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Atm/Bank" class="btn btn-info">Submit</button>
                <a href="{{url('admin/atms')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
  $(function(){
    $('[data-mask]').inputmask()
  })
</script>

@endsection

