@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<style type="text/css">
  
</style>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Pages</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/pages/storepage')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="pageId" class="form-control" id="inputEmail3" placeholder="Page Name" value="@isset($page->id){{$page->id}} @endisset">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Page Name *</label>

                  <div class="col-sm-10">
                    <input type="text" required maxlength="50" name="name" class="form-control" id="inputEmail3" placeholder="Page Name" value="@if(old('name') != '' ){{old('name')}} @elseif(isset($page->name) && old('name') == ''){{$page->name}} @endif">
                    <p style="color:red;">{{ $errors->first('name') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Alias *</label>

                  <div class="col-sm-10">
                    <input type="text" required maxlength="50"  name="alias" class="form-control stripspaces" id="inputPassword3" placeholder="Alias" value="@if(old('alias') != '' ){{old('alias')}}@elseif(isset($page->alias) && old('alias') == ''){{$page->alias}}@endif">

                    <input type="hidden" name="oldAlias" value="@isset($page->alias){{$page->alias}}@endif">
                    <p style="color:red;">{{ $errors->first('alias') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Keywords *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="50"  name="keywords" class="form-control" id="inputPassword3" placeholder="Keywords" value="@if(old('keywords') != '' ){{old('keywords')}} @elseif(isset($page->keywords) && old('keywords') == ''){{$page->keywords}} @endif">
                    <p style="color:red;">{{ $errors->first('keywords') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Title *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="50"  name="title" class="form-control" id="inputPassword3" placeholder="Title" value="@if(old('title') != '' ){{old('title')}} @elseif(isset($page->title) && old('title') == ''){{$page->title}} @endif">
                    <p style="color:red;">{{ $errors->first('title') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Page Content" class="btn btn-info">Submit</button>
                <a href="{{url('admin/pages')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


<script type="text/javascript">
  $(".stripspaces").keyup(function() {
    $(this).val($(this).val().replace(/\s/g, "-"));
    $(this).val($(this).val().toLowerCase());
});
</script>



@endsection

