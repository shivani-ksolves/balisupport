@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addhospital')}}"> + Add Hospital</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage Hospitals</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="HospitalTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Hospital Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Speciality in</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($hospitals as $i) 
                  <tr>
                    <td>{{ $i->name }}</td>
                    <td>{{ $i->email }}</td>
                    <td>{{ $i->phone }}</td>
                    <td>{{ $i->address }}</td>
                    <td>{{ $i->speciality }}</td>
                    
                    <td>
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" title="Disable Hospital" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" title="Enable Hospital" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}"  src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                    </td>
                    <td id="hospitalRow{{$i->id}}">
                      <a data-toggle="tooltip" title="Edit hospital" href="{{url('/admin/hospitals/edit/'.$i->id)}}">
                      <i class="fa fa-fw fa-pencil"></i>
                      </a>
                      <a data-toggle="tooltip" onclick="deleteHospital({{$i->id}})" title="Remove hospital" href="#">
                        <i class="fa fa-fw fa-remove"></i>
                      </a>
                    
                    </td>
                </tr>


                @endforeach

                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
    $('#HospitalTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 6 }
      ]
    });
    
  })
</script>


<script type="text/javascript">
  function deleteHospital(hospitalId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover hospital information",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/hospital/deletehospital')}}",
            data: {hospitalId:hospitalId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! hospital information has been deleted!", {
                icon: "success",
              });
              //$('#pageRow'+pageId).hide();
              var hospitalTable = $('#HospitalTable').DataTable();
              hospitalTable.row( $('#hospitalRow'+hospitalId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(hospitalId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/hospital/changestatus')}}",
            data: {hospitalId:hospitalId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+hospitalId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+hospitalId).attr('title','Disable Hospital').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+hospitalId).attr('title','Enable Hospital').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>

@endsection

