@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addservice')}}"> + Add Service</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage Services</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>


              
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table style="overflow: scroll;" id="ServiceTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Is Paid</th><!-- 
                  <th>Image</th> -->
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($services as $i) 
                  <tr>
                    <td>{{ $i->title }}</td>
                    <td><?php echo $i->description; ?></td>
                    <td>
                      @if($i->is_paid == 1 )
                      Yes 
                      @else 
                      No 
                      @endif
                    </td>
                    <!-- <td>
                      <img height="100px" src="{{ (new \App\Helpers\ImageHelper)->getImage($i->image_name,'service') }}">
                    </td> -->
                    <td>
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Disable service" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Enable service" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                    </td>
                    <td id="serviceRow{{$i->id}}">
                      
                      <a data-toggle="tooltip" title="Edit Service" href="{{url('/admin/services/edit/'.$i->id)}}"><i class="fa fa-fw fa-pencil"></i></a>
                      <a data-toggle="tooltip" onclick="deleteService({{$i->id}})" title="Remove Service" href="#">
                      <i class="fa fa-fw fa-remove"></i>
                      </a>
                    
                    </td>
                </tr>


                @endforeach

                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
    var serviceTable = $('#ServiceTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 4 }
      ]
    });
    
  })
</script>
<script type="text/javascript">
  function deleteService(serviceId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this service",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
            $.ajax({
            type: "POST",
            url: "{{url('/admin/services/deleteservice')}}",
            data: {serviceId:serviceId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! service has been deleted!", {
                icon: "success",
              });
              //$('#pageRow'+pageId).hide();
              var serviceTable = $('#ServiceTable').DataTable();
              serviceTable.row( $('#serviceRow'+serviceId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(serviceId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/services/changestatus')}}",
            data: {serviceId:serviceId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+serviceId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+serviceId).attr('title','Disable Page').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+serviceId).attr('title','Enable Page').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>

@endsection

