@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('adminassets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Holidays</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/holidays/storeholiday')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="holidayId" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="@isset($holiday->id){{$holiday->id}} @endisset">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Holiday Title *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="title" class="form-control" id="inputEmail3" placeholder="Holiday Title" value="@if(old('title') != '' ){{old('title')}} @elseif(isset($holiday->title) && old('title') == ''){{$holiday->title}} @endif">
                    <p style="color:red;">{{ $errors->first('title') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Date *</label>

                  <div class="col-sm-10">
                    <input autocomplete="off" type="text" id="datepicker" required name="holiday_on" class="form-control" id="inputPassword3" placeholder="Holiday On" value="@if(old('holiday_on') != '' ){{old('holiday_on')}} @elseif(isset($holiday->holiday_on) && old('holiday_on') == ''){{$holiday->holiday_on}} @endif">
                    <p style="color:red;">{{ $errors->first('holiday_on') }}</p>
                  </div>
                </div>
                

                
                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Description *</label>

                  <div class="col-sm-10">
                    <input type="" required name="description" class="form-control" id="inputPassword3" placeholder="Description" value="@if(old('description') != '' ){{old('description')}} @elseif(isset($holiday->description) && old('description') == ''){{$holiday->description}} @endif">
                    <p style="color:red;">{{ $errors->first('description') }}</p>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Holiday" class="btn btn-info">Submit</button>
                <a href="{{url('admin/holidays')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
  $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
</script>
@endsection

