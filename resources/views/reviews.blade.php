@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <!-- <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addpages')}}"> + Add Page</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div> -->
            <div class="box-header">
              <h3 class="box-title">Reviews</h3>
              
            </div>


              
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table style="overflow: scroll;" id="ReviewTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Customer</th>
                  <th>Rating</th>
                  <th>Review</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($reviews as $i) 
                  <tr>
                    <td>{{ $i->user['first_name']." ".$i->user['last_name'] }}</td>
                    <td>@for ($x = 0; $x < $i->rating; $x++)
                      <i class="fa fa-fw fa-star"></i>
                      @endfor</td>
                    <td style="text-align: justify;width: 50%;">{{ substr($i->review,0,139) }}@if(strlen($i->review) > 140 )<span style="display: none;" id="read-more-{{$i->id}}"> {{substr($i->review,140,strlen($i->review)-1) }}</span> <a data-toggle="read-more-{{$i->id}}" class="read-more" href="#">Read More</a>@endif</td>
                    <td> 
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Disable Review" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Enable Review" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                    </td>
                    <td id="reviewRow{{$i->id}}">
                      <!-- <a data-toggle="tooltip" title="Edit page" href="{{url('/admin/pages/edit/'.$i->id)}}"><i class="fa fa-fw fa-pencil"></i></a> -->
                      <a data-toggle="tooltip" onclick="deleteReview({{$i->id}})" title="Remove Review" href="#">
                        <i class="fa fa-fw fa-remove"></i>
                      </a>
                    
                    </td>
                </tr>


                @endforeach

                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
    var reviewTable = $('#ReviewTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 4 }
      ]
    });
  })
</script>
<script type="text/javascript">
  function deleteReview(reviewId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this review",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/review/deletereview')}}",
            data: {reviewId:reviewId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              //alert(data);
              //console.log(data);
              swal("Success! review has been deleted!", {
                icon: "success",
              });
              var reviewTable = $('#ReviewTable').DataTable();
              reviewTable.row( $('#reviewRow'+reviewId).parents('tr')).remove().draw();
            }
            });


      }
    });
  }

  function changeStatus(reviewId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/review/changestatus')}}",
            data: {reviewId:reviewId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+reviewId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+reviewId).attr('title','Disable Review').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+reviewId).attr('title','Enable Review').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>
<script type="text/javascript">
  $('.read-more').click(function(){
    //alert($(this).attr('data-toggle'));
    var toggleData = $(this).attr('data-toggle');
    $('#'+toggleData).toggle(100);

    var text = $(this).text();
    if(text == "Read More" ){
      $(this).text("Read Less");
    }else{
      $(this).text("Read More");
    }
  })
</script>

@endsection

