@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Customer</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/user/storeuser')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="userId" value="@isset($users->id){{$users->id}} @endisset">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">First Name *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="first_name" class="form-control" id="inputEmail3" placeholder="First Name" value="@isset($users->first_name){{$users->first_name}} @endisset">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Last Name *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="last_name" class="form-control" id="inputEmail3" placeholder="Last Name" value="@isset($users->last_name){{$users->last_name}} @endisset">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>

                  <div class="col-sm-10">
                    <input type="text" disabled="disabled" required name="email" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="@isset($users->email){{$users->email}} @endisset">
                  </div>
                </div>

                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Phone No. *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="phone" class="form-control" placeholder="Phone No" value="@isset($users->phone){{$users->phone}} @endisset">
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Gender *</label>

                  <div class="col-sm-10">
                    <select name="gender" class="form-control">
                      <option @if($users->gender == 0) selected='selected' @endif value="0">Male</option>
                      <option @if($users->gender == 1) selected='selected' @endif  value="1">Female</option>
                    </select>
                  </div>
                </div>

                <div class="form-group box-header">
                  <label class="col-sm-2 control-label"> <h3 class="box-title">Add Address</h3></label>
                </div>

                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Country *</label>

                  <div class="col-sm-10">
                     <input type="text" disabled="disabled" required name="email" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="Australia">
                  </div>
                </div>

                @foreach($user_address as $user_address)
                <div class="addressToRemove{{$user_address->id}}">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-5">
                    <button type="button" class="btn btn-block btn-primary btn-flat" onclick="removeuseraddress(this)" addressId = '{{$user_address->id}}' >- Remove User Address</button>
                  </div>
                </div>

                <input type="hidden" name="address_id[]" value="{{$user_address->id}}">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">State *</label>

                  <div class="col-sm-10">
                    <select required name="state_id[]" class="form-control">
                      <option @if($user_address->state_id == 1) selected='selected' @endif  value="1">state1</option>
                      <option @if($user_address->state_id == 2) selected='selected' @endif   value="2">state2</option>
                      <option @if($user_address->state_id == 3) selected='selected' @endif   value="3">state3</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">City *</label>

                  <div class="col-sm-10">
                    <select required name="city_id[]" class="form-control">
                      <option @if($user_address->city_id == 1) selected='selected' @endif   value="1">city1</option>
                      <option @if($user_address->city_id == 2) selected='selected' @endif value="2">city2</option>
                      <option @if($user_address->city_id == 3) selected='selected' @endif value="3">city3</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Address *</label>

                  <div class="col-sm-10">
                    <textarea name="address[]" style="margin: 0px; width: 612px; height: 96px;">@if($user_address->address != '') {{$user_address->address}} @endif</textarea>
                  </div>
                </div>
                </div>

                @endforeach




                <div class="addAddress" id="addAddress">
                </div>


                <div class="form-group box-header">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-5">
                  <button type="button" id="addMoreAddress" class="btn btn-block btn-primary btn-flat">+ Add More Address</button>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10" style="color: red;">
                    @if ($errors->any())
                    Please provide all information
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Update Customer" class="btn btn-info">Submit</button>
                <a href="{{url('admin/customers')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
  $(function(){
    $('[data-mask]').inputmask()
  })
</script>

<script type="text/javascript">
  var divId = 1 ; 
  $('#addMoreAddress').click(function(){


    var html = '<div id="addAddressDiv'+divId+'"><div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label"></label><div class="col-sm-5"><button class="btn btn-block btn-primary btn-flat" type="button" onclick="removeaddress(addAddressDiv'+divId+')">Remove Address</button></div></div><div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label">State *</label><div class="col-sm-10"><select name="state_id[]" class="form-control"><option value="1">state1</option><option value="2">state2</option><option value="3">state3</option></select></div></div><div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label">City *</label><div class="col-sm-10"><select name="city_id[]" class="form-control"><option value="1">city1</option><option value="2">city2</option><option value="3">city3</option></select></div></div><div class="form-group"><label for="inputPassword3" class="col-sm-2 control-label">Address *</label><div class="col-sm-10"><textarea name="address[]" style="margin: 0px; width: 612px; height: 96px;"></textarea></div></div></div>';

    $('#addAddress').append(html);

    divId++  ; 


  })

  function removeaddress(removeDiv){
    removeDiv.remove();
  }

  function removeuseraddress(button){
    var addressId = $(button).attr('addressId');
    //alert(addressId);
    $.ajax({
            type: "POST",
            url: "{{url('/admin/user/deleteuseraddress')}}",
            data: {addressId:addressId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('.addressToRemove'+addressId).remove();
            }
            });
  }


</script>

@endsection

