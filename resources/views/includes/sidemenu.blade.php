<!-- Left side column. contains the logo and sidebar -->
  
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('adminassets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Session::get('name') }}</p>          
        </div>
      </div>
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class="">
          <a href="{{url('/admin/customers')}}">
            <i class="fa fa-fw fa-users"></i>
            <span>Customers</span>
          </a>
        </li>


        <li class="">
          <a href="{{url('/admin/pages')}}">
            <i class="fa fa-files-o"></i>
            <span>Pages</span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-exchange"></i>
            <span>Services</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('/admin/services')}}"><i class="fa fa-fw fa-plus-circle"></i> Services</a></li>
            <!-- <li><a href="#"><i class="fa fa-circle-o"></i>Volcano Status</a></li> -->
            <li><a href="{{url('/admin/hospitals')}}"><i class="fa fa-fw fa-ambulance"></i> Hospitals</a></li>
            <!-- <li><a href="{{url('/admin/holidays')}}"><i class="fa fa-circle-o"></i> Holidays</a></li> -->
             <li><a href="{{url('/admin/atms')}}"><i class="fa fa-fw fa-money"></i> ATMs/Banks</a></li>
             <li><a href="{{url('/admin/holidays')}}"><i class="fa fa-fw fa-ambulance"></i> Holidays</a></li>
             <li><a href="{{url('/admin/travelAlerts')}}"><i class="fa fa-fw fa-circle"></i> Travel Alerts</a></li>
             
          </ul>
        </li>

        <li class="">
          <a href="{{url('/admin/faq')}}">
            <i class="fa fa-fw fa-question"></i>
            <span>FAQ</span>
          </a>
        </li>

       <li class="">
          <a href="{{url('/admin/query')}}">
            <i class="fa  fa-question-circle"></i>
            <span>Queries</span>
          </a>
        </li>

        <li class="">
          <a href="{{url('/admin/review')}}">
            <i class="fa fa-fw fa-star-half-empty"></i>
            <span>Reviews</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->