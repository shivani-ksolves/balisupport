@extends('layouts.basiclayout')
@section('content')
  <div class="login-logo">
    <a href="../../index2.html"><b>Bali </b>Support Center</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
     <p>
    
    @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach

    </p>
    <p class="login-box-msg">Sign in</p>

    <form action="{{ URL::Route('authenticate') }}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <a href="#">I forgot my password</a><br>
          <a href="register.html" class="text-center">Register a new membership</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>



  </div>
  <!-- /.login-box-body -->
@endsection
