@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addfaq')}}"> + Add Questions</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage FAQ</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>


              
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="FaqTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Question</th>
                  <th>Status</th>
                  <th>Action</th>
                  <!-- <th></th> -->
                </tr>
                </thead>
                <tbody>
                  @foreach ($faq as $i)
                <tr>
                   
                  <td>{{ $i->question }}</td>
                 
                  <td>
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Disable FAQ" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Enable FAQ" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                  </td>
                  <td id="faqRow{{$i->id}}">
                      
                      <a data-toggle="tooltip" title="Edit question" href="{{url('/admin/faq/edit/'.$i->id)}}"><i class="fa fa-fw fa-pencil"></i></a>
                      <a data-toggle="tooltip" onclick="deleteFaq({{$i->id}})"  title="Remove question" href="#">
                      <i class="fa fa-fw fa-remove"></i>
                      </a>
                     
                  </td>
                  <!-- <td><button type="button" class="btn btn-block btn-primary btn-sm">Detail</button></td> -->
                </tr>
                @endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->
<script>
  $(function () {
    var pageTable = $('#FaqTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 2 }
      ]
    });
    
  });
</script>
<script type="text/javascript">

  function deleteFaq(faqId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this quesion",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/faq/deletefaq')}}",
            data: {faqId:faqId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              if(data == 'true'){
                swal("Success! Your question has been deleted!", {
                  icon: "success",
                });
                var pageTable = $('#FaqTable').DataTable();
                pageTable.row( $('#faqRow'+faqId).parents('tr')).remove().draw();

              } else {
                swal("Error! Some error occurred, please try again!", {
                  icon: "danger",
                });
              }
            }
            });
      }
    });
  }

  function changeStatus(faqId){
    $.ajax({
        type: "POST",
        url: "{{url('/admin/faq/changestatus')}}",
        data: {faqId:faqId},
        cache: false,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data){
          $('#statusImage'+faqId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
          swal("Thanks ! Your status has been udpated", {
            icon: "success",
          });
          if(data == 'activegreen.png'){
            $('#changeStatus'+faqId).attr('title','Disable FAQ').tooltip('fixTitle').tooltip('setContent');
          }else{
            $('#changeStatus'+faqId).attr('title','Enable FAQ').tooltip('fixTitle').tooltip('setContent');
          }
          swal("Thanks ! Status has been udpated", {
            icon: "success",
          });
        }
        });
  }
</script>



@endsection

