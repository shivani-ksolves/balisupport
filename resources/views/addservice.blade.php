@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<style type="text/css">
  
</style>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Service</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/services/storeservices')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="serviceId" value="@isset($service->id){{$service->id}} @endisset">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Title *</label>

                  <div class="col-sm-10">
                    <input type="text" maxlength="50" required name="title" class="form-control" id="title" placeholder="Service Title" value="@if(old('title') != '' ){{old('title')}}@elseif(isset($service->title) && old('title') == ''){{$service->title}}@endif">
                    <p style="color:red;">{{ $errors->first('title') }}</p>
                    <input type="hidden" name="old_title" value="@isset($service->title){{$service->title}}@endisset" >
                  </div>
                </div>

                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Description *</label>

                  <div class="col-sm-10">


                    <textarea id="ContentEditor" required name="description" rows="10" cols="80">
                      @if(old('description') != '' ){{old('description')}} @elseif(isset($service->description) && old('description') == ''){{$service->description}} @endif
                    </textarea>


                    <!-- <input type="text" maxlength="140" required name="description" class="form-control" id="description" placeholder="Service Description" value=""> -->

                    <p style="color:red;">{{ $errors->first('description') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Is Paid *</label>

                  <div class="col-sm-10">
                    <?php 
                    $yes = '';
                    $no = '' ;
                    if(isset($service->is_paid)){
                      if($service->is_paid == '1' ){
                        $yes = 'checked="checked"';
                      }else{
                        $no = 'checked="checked"';
                      }
                    }
                    ?>
                    <input {{$yes}} class="" name="is_paid" value="1" type="radio"> <label>Yes</label><br>
                    <input {{$no}} class="" name="is_paid" value="0" type="radio"> <label>No</label>
                    <p style="color:red;">{{ $errors->first('is_paid') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Image *</label>

                  <div class="col-sm-10">
                    <input type="file" maxlength="140" name="service_image"  value="">

                    <p style="color:red;">{{ $errors->first('service_image') }}</p>
                  </div>
                </div>

                @isset($service->image_name)

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"><!-- Old Image * --></label>

                  <div class="col-sm-10">
                    <img height="100px" src="{{ (new \App\Helpers\ImageHelper)->getImage($service->image_name,'service') }}">
                  </div>
                </div>

                <input type="hidden" name="old_image" value="{{$service->image_name}}">
                @endisset
                

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Page Content" class="btn btn-info">Submit</button>
                <a href="{{url('admin/services')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


<script type="text/javascript">
  $(".stripspaces").keyup(function() {
    $(this).val($(this).val().replace(/\s/g, "-"));
    $(this).val($(this).val().toLowerCase());
});


</script>


<!-- To add the script for the CKEditor -->
<script src="{{asset('adminassets/bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- Calling and initialization of CKEditor -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    var editor  = CKEDITOR.replace('ContentEditor');
    //bootstrap WYSIHTML5 - text editor
  })
</script>

@endsection

