@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Setting</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/profile/updateinfo')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
              <div class="box-body">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Admin Name *</label>

                  <div class="col-sm-10">
                    <input type="text" disabled="disabled"  maxlength="50"  name="name" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="{{ Session::get('name') }}">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Contact email *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="50"  name="email" class="form-control" id="inputEmail3" placeholder="Contact Email" value="@if(old('email') != '' ){{old('email')}} @elseif(isset($settings->email) && old('email') == ''){{$settings->email}} @endif">
                    <p style="color:red;">{{ $errors->first('email') }}</p>
                  </div>
                </div>

                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Phone no. *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="140"  name="phone" class="form-control" id="inputPassword3" placeholder="Address" value="@if(old('phone') != '' ){{old('phone')}} @elseif(isset($settings->phone) && old('phone') == ''){{$settings->phone}} @endif">
                    <p style="color:red;">{{ $errors->first('phone') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Office Address *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="20"  name="office_address" class="form-control"  placeholder="Phone No" value="@if(old('office_address') != '' ){{old('office_address')}} @elseif(isset($settings->office_address) && old('office_address') == ''){{$settings->office_address}} @endif">
                    <p style="color:red;">{{ $errors->first('office_address') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Longitude *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="250"  name="office_longitude" class="form-control" id="inputPassword3" placeholder="Email" value="@if(old('office_longitude') != '' ){{old('office_longitude')}}@elseif(isset($settings->office_longitude) && old('office_longitude') == ''){{$settings->office_longitude}}@endif">
                    <p style="color:red;">{{ $errors->first('office_longitude') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Lattitude *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="140"  name="office_latitude" class="form-control" id="inputPassword3" placeholder="Speciality In" value="@if(old('office_latitude') != '' ){{old('office_latitude')}}@elseif(isset($settings->office_latitude) && old('office_latitude') == ''){{$settings->office_latitude}}@endif">
                    <p style="color:red;">{{ $errors->first('office_latitude') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Hospital" class="btn btn-info">Submit</button>
                <a href="{{url('admin/hospitals')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
  $(function(){
    $('[data-mask]').inputmask()
  })
</script>

@endsection

