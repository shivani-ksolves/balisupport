@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Manage Query</h3>

              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="QueryTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone No.</th>
                  <th>Subject</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 @foreach ($query as $i) 
                  <tr>
                    <td>{{ $i->first_name }} {{ $i->last_name }}</td>
                    <td>{{ $i->email }}</td>
                    <td>{{ $i->phone }}</td>
                    <td>{{ $i->subject}}</td>
                    <td>
                      @if($i->status == 1 )
                          <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Disable query" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                          @else
                          <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Enable query" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                      @endif
                    </td>
                    <td id="queryRow{{$i->id}}">
                      <a data-toggle="tooltip" title="Detail" data-toggle="tooltip" onclick="viewQuery({{$i->id}})" href="{{url('/admin/query/querycontent/'.$i->id)}}"><i class="glyphicon glyphicon-info-sign"></i></a>

                      <a data-toggle="tooltip" title="Remove query" data-toggle="tooltip" onclick="deleteQuery({{$i->id}})" href="#"><i class="fa fa-fw fa-remove"></i></a>
                    </td>
                  </tr>
                 @endforeach
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
   $('#QueryTable').DataTable()
    
  })
</script>
<script type="text/javascript">

  function deleteQuery(queryId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this user",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/query/deletequery')}}",
            data: {queryId:queryId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! Query has been deleted!", {
                icon: "success",
              });
              //$('#pageRow'+pageId).hide();
              var queryTable = $('#QueryTable').DataTable();
              queryTable.row( $('#queryRow'+queryId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(queryId){ 
    $.ajax({
            type: "POST",
            url: "{{url('/admin/query/changestatus')}}",
            data: {queryId:queryId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){ 
              console.log(data);
              $('#statusImage'+queryId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+queryId).attr('title','Disable Query').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+queryId).attr('title','Enable query').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>



@endsection

