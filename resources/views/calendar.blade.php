@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/dist/css/fullcalendar.min.css')}}">

<!-- <link rel="stylesheet" href="{{asset('adminassets/dist/css/fullcalendar.print.min.css')}}"> -->
<style type="text/css">
   .datepicker-days .table-condensed{
    display: none;
   }

</style>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addholiday')}}"> + Add Holidays</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>

              <div class="col-xs-12">
                 <div id='calendar'></div>
              </div>

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


<!-- <script src='https://fullcalendar.io/releases/fullcalendar/3.9.0/lib/jquery.min.js' type="text/javascript"></script> -->
<script src="{{asset('adminassets/dist/js/moment.min.js')}}"></script>
<script src="{{asset('adminassets/dist/js/calendar.min.js')}}"></script>

<script>

  $(document).ready(function() {
    //console.log(<?php echo html_entity_decode($holidays) ?>);
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },

      defaultDate: '2018-03-12',
      navLinks: true, // can click day/week names to navigate views
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: <?php echo $holidays ?>
    });

  });



</script>




@endsection

