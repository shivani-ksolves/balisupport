@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">View Query</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="pageId" value="{{$id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Subject</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" disabled="disabled" value="{{$query->subject}}" class="form-control" id="inputEmail3" placeholder="Page Name" readonly="readonly">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Query</label>

                  <div class="col-sm-10">
                    <textarea id="" required name="pageContent" rows="10" cols="80" readonly="readonly">
                      @isset($query->query){{$query->query}}@endisset
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10" style="color: red;">
                    @if ($errors->any())
                    Please provide content for the page.
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <a  href="{{url('admin/query')}}" class="btn btn-default">Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    



@endsection


