@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Pages</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/pages/updatecontent')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="pageId" value="{{$id}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Page Name</label>

                  <div class="col-sm-10">
                    <input type="text" name="name" disabled="disabled" value="{{$page->name}}" class="form-control" id="inputEmail3" placeholder="Page Name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Content</label>

                  <div class="col-sm-10">
                    <textarea id="ContentEditor" required name="pageContent" rows="10" cols="80">
                      @isset($page->content){{$page->content}}@endisset
                    </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10" style="color: red;">
                    @if ($errors->any())
                    Please provide content for the page.
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a  href="{{url('admin/pages')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <form action="{{url('admin/pages/uploadimage')}}" id="imageForm" method="POST" enctype="multipart/form-data">
        <input type="file" name="image_to_upload" id="imgupload" style="display:none"/> 
      </form>
      <!-- /.row -->
    </section>




    
<!-- To add the script for the CKEditor -->
<script src="{{asset('adminassets/bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- Calling and initialization of CKEditor -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    var editor  = CKEDITOR.replace('ContentEditor');

    editor.addCommand("mySimpleCommand", { // create named command
    exec: function(edt) {
        $('#imgupload').trigger('click'); 
        //alert(edt.getData());
    }
    });

    editor.ui.addButton('SuperButton', { // add new button and bind our command
        label: "Upload image from disk",
        command: 'mySimpleCommand',
        toolbar: 'insert',
        icon: 'http://simpleicon.com/wp-content/uploads/cloud-upload-1.svg'
    });

    //bootstrap WYSIHTML5 - text editor
  })

  $('#imgupload').change(function(){
      //ert("A file has been selected.");
     
        var formData = new FormData($('#imageForm')[0]);
        var url = $('#imageForm').attr('action');
        $.ajax({
            url:url,
            type: "POST",
            data: formData,
            async: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            success: function (msg) {
                //alert(msg)
                var html = '<p><img src="'+msg+'"></p>';
                CKEDITOR.instances['ContentEditor'].insertElement(html);
            },
            cache: false,
            contentType: false,
            processData: false
        });

        e.preventDefault();

  });



</script>



@endsection


