@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addpages')}}"> + Add Page</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage Pages</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>


              
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table style="overflow: scroll;" id="PageTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Page Name</th>
                  <th>Alias</th>
                  <th>Keywords</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($pages as $i) 
                  <tr>
                    <td>{{ $i->name }}</td>
                    <td>{{ $i->alias }}</td>
                    <td>{{ $i->keywords }}</td>
                    <td>{{ $i->title }}</td>
                    <td>
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Disable page" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})" title="Enable page" class="btn"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                    </td>
                    <td id="pageRow{{$i->id}}">
                      
                      <a data-toggle="tooltip" title="Edit page" href="{{url('/admin/pages/edit/'.$i->id)}}"><i class="fa fa-fw fa-pencil"></i></a>
                      <a data-toggle="tooltip" onclick="deletePage({{$i->id}})" title="Remove page" href="#">
                      <i class="fa fa-fw fa-remove"></i>
                      </a>
                      <a data-toggle="tooltip" title="Update page content" href="{{url('/admin/pages/pagecontent/'.$i->id)}}"><i class="fa fa-fw fa-plus-square-o"></i></a>
                    
                    </td>
                </tr>


                @endforeach

                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
    var pageTable = $('#PageTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 5 }
      ]
    });
    
  })
</script>

<script type="text/javascript">

  

  function deletePage(pageId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this page",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/pages/deletepage')}}",
            data: {pageId:pageId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! page has been deleted!", {
                icon: "success",
              });
              //$('#pageRow'+pageId).hide();
              var pageTable = $('#PageTable').DataTable();
              pageTable.row( $('#pageRow'+pageId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(pageId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/pages/changestatus')}}",
            data: {pageId:pageId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+pageId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+pageId).attr('title','Disable Page').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+pageId).attr('title','Enable Page').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>

@endsection

