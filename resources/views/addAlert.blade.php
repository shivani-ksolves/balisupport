@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('adminassets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Travel Alert</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/alert/storeAlerts')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="alertId" class="form-control" id="alertId" value="@isset($alert->id){{$alert->id}} @endisset">
              <div class="box-body">

                <div class="form-group">
                  <label for="datepicker" class="col-sm-2 control-label">Start Date *</label>

                  <div class="col-sm-10">
                    <input autocomplete="off" type="text" id="datepicker" name="start_date" class="form-control datepicker" placeholder="Start Date" value="@if(old('start_date') != '' ){{old('start_date')}} @elseif(isset($alert->start_date) && old('start_date') == ''){{$alert->start_date}} @endif">
                    <p style="color:red;">{{ $errors->first('start_date') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="enddatepicker" class="col-sm-2 control-label">End Date</label>

                  <div class="col-sm-10">
                    <input autocomplete="off" type="text" id="enddatepicker" name="end_date" class="form-control datepicker" placeholder="End Date (Optional)" value="@if(old('end_date') != '' ){{old('end_date')}} @elseif(isset($alert->end_date) && old('end_date') == ''){{$alert->end_date}} @endif">
                    <p style="color:red;">{{ $errors->first('end_date') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Title *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="title" class="form-control" id="title" placeholder="Title" value="@if(old('title') != '' ){{old('title')}} @elseif(isset($alert->title) && old('title') == ''){{$alert->title}} @endif">
                    <p style="color:red;">{{ $errors->first('title') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description *</label>

                  <div class="col-sm-10">

                    <textarea required name="description" class="form-control" id="description" placeholder="Description">@if(isset($alert->description)){{$alert->description}} @endif</textarea>
                    <p style="color:red;">{{ $errors->first('description') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="@if(isset($alert->id) )Edit Alert @else Add Alert @endif" class="btn btn-info">Submit</button>
                <a href="{{url('admin/travelAlerts')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
  $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
</script>
@endsection

