@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Hospital</h3>
            </div>
                
                <!-- Calendar -->
                <div class="box box-solid bg-green-gradient">
                  <div class="box-header">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">Calendar</h3>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                      <!-- button with a dropdown -->
                      <div class="btn-group">
                        <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                          <i class="fa fa-bars"></i></button>
                        <ul class="dropdown-menu pull-right" role="menu">
                          <li><a href="#">Add new event</a></li>
                          <li><a href="#">Clear events</a></li>
                          <li class="divider"></li>
                          <li><a href="#">View calendar</a></li>
                        </ul>
                      </div>
                      <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                    <!-- /. tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <!--The calendar -->
                    <div id="calendar" style="width: 100%"></div>
                  </div>
                  <!-- /.box-body -->
                  
                </div>
                <!-- /.box -->


          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
  $(function(){
    $('[data-mask]').inputmask()
  })
</script>

@endsection

