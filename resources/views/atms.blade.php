@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addatms')}}"> + Add Atms/Banks</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage Atms/Banks</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="AtmsTable" class="table table-bordered table-striped">
                <thead>


                <tr>
                  <th>Bank Name</th>
                  <th>Type</th>
                  <th>Phone No.</th>
                  <th>Address</th>
                  <th>Longitude</th>
                  <th>Lattitude</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($atms as $i) 
                  <tr>
                    <td>{{ $i->bankname }}</td>

                    <td>@if($i->type == 1 ) ATM @else Bank @endif</td>


                    <td>{{ $i->phone }}</td>
                    <td>{{ $i->address }}</td>
                    <td>{{ $i->long }}</td>
                    <td>{{ $i->lat }}</td>
                    <td>
                    @if($i->status == 1 )
                    <button data-toggle="tooltip" title="Disable" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                    @else
                    <button data-toggle="tooltip" title="Enable" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}"  src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                    @endif
                    </td>
                    <td id="atmsRow{{$i->id}}">
                      <a data-toggle="tooltip" title="Edit" href="{{url('/admin/atm/edit/'.$i->id)}}">
                      <i class="fa fa-fw fa-pencil"></i>
                      </a>
                      <a data-toggle="tooltip" onclick="deleteAtm({{$i->id}})" title="Remove" href="#">
                        <i class="fa fa-fw fa-remove"></i>
                      </a>
                    
                    </td>
                </tr>


                @endforeach

                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    


<!-- SlimScroll -->


<script>
  $(function () {
    $('#AtmsTable').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 6 }
      ]
    });
    
  })
</script>


<script type="text/javascript">
  function deleteAtm(atmId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover information",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/atms/deleteatm')}}",
            data: {atmId:atmId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! Information has been deleted!", {
                icon: "success",
              });
              //$('#pageRow'+pageId).hide();
              var atmTable = $('#AtmsTable').DataTable();
              atmTable.row( $('#atmsRow'+atmId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(atmId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/atms/changestatus')}}",
            data: {atmId:atmId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+atmId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+atmId).attr('title','Disable').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+atmId).attr('title','Enable').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
            });
  }
</script>

@endsection

