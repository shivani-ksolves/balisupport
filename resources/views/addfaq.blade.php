@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
<style type="text/css">
  
</style>
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Questions</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/faq/storefaq')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="faqId" class="form-control" id="inputEmail3" value="@isset($faq->id){{$faq->id}} @endisset">

            <input type="hidden" name="created_by" class="form-control" id="userId" value="{{Session::get('id')}}">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Question *</label>

                  <div class="col-sm-10">
                    <input type="text" required name="question" class="form-control" id="inputEmail3" placeholder="Enter Question" value="@isset($faq->question){{$faq->question}} @endisset">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Answer*</label>

                  <div class="col-sm-10">
                      <textarea id="ContentEditor" required name="answer" rows="10" cols="80" placeholder="Enter your answer">
                        @isset($faq->answer){{$faq->answer}}@endisset
                    </textarea>
                  </div>
                </div>
              
               
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10" style="color: red;">
                    @if ($errors->any())
                    Please provide all information
                    @endif
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add faq Content" class="btn btn-info">Submit</button>
                <a href="{{url('admin/faq')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

<!-- To add the script for the CKEditor -->
<script src="{{asset('adminassets/bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- Calling and initialization of CKEditor -->

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('ContentEditor')
    //bootstrap WYSIHTML5 - text editor
  })
</script>



@endsection


