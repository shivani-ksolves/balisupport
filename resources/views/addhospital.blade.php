@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-9">
          

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Hospital</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{url('admin/hospital/storehospital')}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="hospitalId" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="@isset($hospitals->id){{$hospitals->id}} @endisset">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Hospital Name *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="50"  name="name" class="form-control" id="inputEmail3" placeholder="Hospital Name" value="@if(old('name') != '' ){{old('name')}} @elseif(isset($hospitals->name) && old('name') == ''){{$hospitals->name}} @endif">
                    <p style="color:red;">{{ $errors->first('name') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Address *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="140"  name="address" class="form-control" id="inputPassword3" placeholder="Address" value="@if(old('address') != '' ){{old('address')}} @elseif(isset($hospitals->address) && old('address') == ''){{$hospitals->address}} @endif">
                    <p style="color:red;">{{ $errors->first('address') }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Phone No. *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="20"  name="phone" class="form-control"  placeholder="Phone No" value="@if(old('phone') != '' ){{old('phone')}} @elseif(isset($hospitals->phone) && old('phone') == ''){{$hospitals->phone}} @endif">
                    <p style="color:red;">{{ $errors->first('phone') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Email *</label>

                  <div class="col-sm-10">
                    <input type="email" required  maxlength="250"  name="email" class="form-control" id="inputPassword3" placeholder="Email" value="@if(old('email') != '' ){{old('email')}} @elseif(isset($hospitals->email) && old('email') == ''){{$hospitals->email}} @endif">
                    <p style="color:red;">{{ $errors->first('email') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Speciality In *</label>

                  <div class="col-sm-10">
                    <input type="text" required  maxlength="140"  name="speciality" class="form-control" id="inputPassword3" placeholder="Speciality In" value="@if(old('speciality') != '' ){{old('speciality')}} @elseif(isset($hospitals->speciality) && old('speciality') == ''){{$hospitals->speciality}} @endif">
                    <p style="color:red;">{{ $errors->first('speciality') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Longitude *</label>

                  <div class="col-sm-10">
                    <input type="" required maxlength="30" name="long" class="form-control" id="inputPassword3" placeholder="Longitude" value="@if(old('long') != '' ){{old('long')}} @elseif(isset($hospitals->long) && old('long') == ''){{$hospitals->long}} @endif">
                    <p style="color:red;">{{ $errors->first('long') }}</p>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Latitude *</label>

                  <div class="col-sm-10">
                    <input type="" required maxlength="30" name="lat" class="form-control" id="inputPassword3" placeholder="Latitude" value="@if(old('lat') != '' ){{old('lat')}} @elseif(isset($hospitals->lat) && old('lat') == ''){{$hospitals->lat}} @endif">
                    <p style="color:red;">{{ $errors->first('lat') }}</p>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    <p>* fields  are mendatory</p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" data-toggle="tooltip" title="Add Hospital" class="btn btn-info">Submit</button>
                <a href="{{url('admin/hospitals')}}" class="btn btn-default">Cancel</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

    
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{asset('adminassets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script type="text/javascript">
  $(function(){
    $('[data-mask]').inputmask()
  })
</script>

@endsection

