@extends('layouts.layout')
@section('content')

<link rel="stylesheet" href="{{asset('adminassets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <!-- /.box -->

          <div class="box">
            <div class="container" style="margin-top:20px;">
              <div class="col-md-3">
              </div>
              <div class="col-md-4">
              <div class="">
              <a class="btn btn-block btn-primary btn-flat" href="{{url('/admin/addAlerts')}}"> + Add Travel Alerts</a>
              </div>
              </div>
              <div class="col-md-5">
              </div>
            </div>
            <div class="box-header">
              <h3 class="box-title">Manage Travel Alerts</h3>
              @if(Session::get('success') != '')
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i>{{Session::get('success')}}</h4>
                
              </div>
              {{ Session::forget('success') }}
              @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="addAlerts" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Start Date</th>
                  <th>End Date</th>   
                  <th>Status</th>               
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach ($alerts as $i) 
                  <tr>
                    <td>{{ $i->title }}</td>

                    <td style="text-align: justify;width: 40%;">{{ substr($i->description,0,139) }}@if(strlen($i->description) > 140 )<span style="display: none;" id="read-more-{{$i->id}}"> {{substr($i->description,140,strlen($i->description)-1) }}</span> <a data-toggle="read-more-{{$i->id}}" class="read-more" href="#">Read More</a>@endif</td>

                    <td>{{ $i->start_date }}</td>
                    <td>{{ $i->end_date }}</td>

                    <td>
                      @if($i->status == 1 )
                      <button data-toggle="tooltip" title="Disable" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}" src="{{asset('adminassets/dist/img/activegreen.png')}}"></button>
                      @else
                      <button data-toggle="tooltip" title="Enable" class="btn" id="changeStatus{{$i->id}}" onclick="changeStatus({{$i->id}})"><img height="20px" id="statusImage{{$i->id}}"  src="{{asset('adminassets/dist/img/inactivered.png')}}"></button>
                      @endif
                    </td>

                    <td id="alertRow{{$i->id}}">
                      <a data-toggle="tooltip" title="Edit" href="{{url('/admin/alert/edit/'.$i->id)}}">
                      <i class="fa fa-fw fa-pencil"></i>
                      </a>
                      <a data-toggle="tooltip" onclick="deleteAlert({{$i->id}})" title="Remove" href="#">
                        <i class="fa fa-fw fa-remove"></i>
                      </a>                    
                    </td>
                  </tr>

                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<!-- SlimScroll -->
<script>

  $('.read-more').click(function(){
    //alert($(this).attr('data-toggle'));
    var toggleData = $(this).attr('data-toggle');
    $('#'+toggleData).toggle(100);

    var text = $(this).text();
    if(text == "Read More" ){
      $(this).text("Read Less");
    }else{
      $(this).text("Read More");
    }
  })

  $(function () {
    $('#addAlerts').DataTable({
      "columnDefs": [
        { "orderable": false, "targets": 5  }
      ]
    });
    
  })
</script>


<script type="text/javascript">
  function deleteAlert(alertId){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover information",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {


            $.ajax({
            type: "POST",
            url: "{{url('/admin/alert/deleteAlert')}}",
            data: {alertId:alertId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              swal("Success! Alert has been deleted!", {
                icon: "success",
              });
              var alertTable = $('#addAlerts').DataTable();
              alertTable.row( $('#alertRow'+alertId).parents('tr')).remove().draw();
            }
            });
      }
    });
  }

  function changeStatus(alertId){
    $.ajax({
            type: "POST",
            url: "{{url('/admin/alert/changeStatus')}}",
            data: {alertId:alertId},
            cache: false,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
              $('#statusImage'+alertId).attr('src','{{asset("adminassets/dist/img/")}}'+'/'+data);
              if(data == 'activegreen.png'){
                $('#changeStatus'+alertId).attr('title','Disable').tooltip('fixTitle').tooltip('setContent');
              }else{
                $('#changeStatus'+alertId).attr('title','Enable').tooltip('fixTitle').tooltip('setContent');
              }
              swal("Thanks ! status has been udpated", {
                icon: "success",
              });
            }
    });
  }

</script>

@endsection

